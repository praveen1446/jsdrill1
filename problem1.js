function findCarById(inventory, id) {
    for (let value = 0; value < inventory.length; value++) {
        if (inventory[value].id === id) {
            return inventory[value];
        }
    }
    return null;
}
module.exports = findCarById;

