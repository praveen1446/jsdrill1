//problem5//
function getOlderCars(inventory) {
    const olderCars = [];
    for (let index = 0; index < inventory.length; index++) {
        if (inventory[index].car_year < 2000) {
            olderCars.push(inventory[index]);
        }
    }
    return olderCars;
}
module.exports = getOlderCars;