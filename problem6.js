//problem6//
function filterBMWAndAudi(inventory) {
    const BMWAndAudi = [];
    for (let index = 0; index < inventory.length; index++) {
        if (inventory[index].car_make === 'BMW' || inventory[index].car_make === 'Audi') {
            BMWAndAudi.push(inventory[index]);
        }
    }
    return BMWAndAudi;
}
module.exports = filterBMWAndAudi;
