// problem3.js//
function sortCarModels(inventory) {
    const models = [];
    for (let index = 0; index < inventory.length; index++) {
        models.push(inventory[index].car_model);
    }
    models.sort();
    return models;
}
module.exports = sortCarModels;
